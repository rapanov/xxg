/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef XXG_TRANSFORMER_HH
#define XXG_TRANSFORMER_HH

#include <xxg/math.hh>

namespace xxg
{

struct Transformer
{
  Transformer() noexcept
    : m_0_0_(1.0)
    , m_0_1_(0.0)
    , m_0_2_(0.0)
    , m_1_0_(0.0)
    , m_1_1_(1.0)
    , m_1_2_(0.0)
  {}

  void transform(double& x, double& y) const
  {
    const double new_x = m_0_0_ * x + m_0_1_ * y + m_0_2_;
    const double new_y = m_1_0_ * x + m_1_1_ * y + m_1_2_;
    x = new_x;
    y = new_y;
  }

  void reset() noexcept
  {
    *this = Transformer();
  }

  void translate(const double t_x, const double t_y)
  {
    m_0_2_ += t_x;
    m_1_2_ += t_y;
  }

  void rotate(
    const double theta,
    const double center_x = 0.0,
    const double center_y = 0.0)
  {
    translate(-center_x, -center_y);

    const double cos = Math::cos(theta);
    const double sin = Math::sin(theta);
    const double new_m_0_0 = m_0_0_ * cos + m_0_1_ * sin;
    const double new_m_0_1 = - m_0_0_ * sin + m_0_1_ * cos;
    const double new_m_1_0 = m_1_0_ * cos + m_1_1_ * sin;
    const double new_m_1_1 = -m_1_0_ * sin + m_1_1_ * cos;
    m_0_0_ = new_m_0_0;
    m_0_1_ = new_m_0_1;
    m_1_0_ = new_m_1_0;
    m_1_1_ = new_m_1_1;

    translate(center_x, center_y);
  }

private:
  double m_0_0_;
  double m_0_1_;
  double m_0_2_;
  double m_1_0_;
  double m_1_1_;
  double m_1_2_;
};

} // namespace xxg

#endif // XXG_TRANSFORMER_HH
