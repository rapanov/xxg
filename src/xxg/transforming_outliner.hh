/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef XXG_TRANSFORMINGOUTLINER_HH
#define XXG_TRANSFORMINGOUTLINER_HH


#include <xxg/transformer.hh>

namespace xxg
{

template <class O>
struct Transforming_outliner
{
  using Outliner = O;
  using Transformer = ::xxg::Transformer;

  explicit Transforming_outliner(Outliner&& outliner) noexcept
    : outliner_(static_cast<Outliner&&>(outliner))
  {}

  const Transformer& transformer() const noexcept
  {
    return transformer_;
  }

  Transformer& transformer() noexcept
  {
    return transformer_;
  }

  void move_to(double x, double y) const
  {
    transformer_.transform(x, y);
    static_cast<Outliner&&>(outliner_).move_to(x, y);
  }

  void line_to(double x, double y) const
  {
    transformer_.transform(x, y);
    static_cast<Outliner&&>(outliner_).line_to(x, y);
  }

private:
  Outliner&&  outliner_;
  Transformer transformer_;
};

} // namespace xxg

#endif // XXG_TRANSFORMINGOUTLINER_HH
