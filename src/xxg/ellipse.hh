/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef XXG_ELLIPSE_HH
#define XXG_ELLIPSE_HH

#include <xxg/math.hh>

namespace xxg
{

struct Ellipse
{
  Ellipse(
    const double center_x,
    const double center_y,
    double radius_x,
    double radius_y,
    const bool ccw = false)
    : center_x_(center_x)
    , center_y_(center_y)
    , ccw_(ccw)
  {
    radius_x = Math::fabs(radius_x);
    radius_y = Math::fabs(radius_y);
    radius_x_ = radius_x;
    radius_y_ = radius_y;

    if (radius_x != 0.0 || radius_y != 0.0)
    {
      const double rad =
        radius_x == radius_y ? radius_x : (radius_x + radius_y) * 0.5;
      double ang_step = Math::acos(rad / (rad + 0.125)) * 2.0;
      const double step_count = Math::floor(Math::pi_x_2 / ang_step + 0.5);
      ang_step = Math::pi_x_2 / step_count;

      ang_step_ = ang_step;
      cos_ang_step_ = Math::cos(ang_step);
      sin_ang_step_ = Math::sin(ang_step);
    }
    else
    {
      ang_step_ = 0.0;
      cos_ang_step_ = 1.0;
      sin_ang_step_ = 0.0;
    }
  }

  template <class Outliner>
  void outline(
    Outliner&& outliner, const bool close = true) const
  {
    const double center_x = center_x_;
    const double center_y = center_y_;
    const double radius_x = radius_x_;
    const double radius_y = radius_y_;
    const double ang_step = ang_step_;
    const double cos_ang_step = cos_ang_step_;
    const double sin_ang_step = sin_ang_step_;

    const double x_0 = center_x + radius_x;
    double ang, next_cos;
    double cos = 1.0;
    double sin = 0.0;
    static_cast<Outliner&&>(outliner).move_to(x_0, center_y);

    if (ccw_)
    {
      for (ang = Math::pi_x_2 - ang_step; ; ang -= ang_step)
      {
        if (ang > 0.0)
        {
          next_cos = cos * cos_ang_step + sin * sin_ang_step;
          sin = sin * cos_ang_step - cos * sin_ang_step;
          cos = next_cos;
          static_cast<Outliner&&>(outliner).line_to(
            center_x + radius_x * cos, center_y + radius_y * sin);
        }
        else
        {
          if (close)
          {
            static_cast<Outliner&&>(outliner).line_to(x_0, center_y);
          }

          break;
        }
      }
    }
    else
    {
      for (ang = ang_step; ; ang += ang_step)
      {
        if (ang < Math::pi_x_2)
        {
          next_cos = cos * cos_ang_step - sin * sin_ang_step;
          sin = sin * cos_ang_step + cos * sin_ang_step;
          cos = next_cos;
          static_cast<Outliner&&>(outliner).
            line_to(center_x + radius_x * cos, center_y + radius_y * sin);
        }
        else
        {
          if (close)
          {
            static_cast<Outliner&&>(outliner).line_to(x_0, center_y);
          }

          break;
        }
      }
    }
  }

private:
  using Math = ::xxg::Math;

  double center_x_;
  double center_y_;
  double radius_x_;
  double radius_y_;
  double ang_step_;
  double cos_ang_step_;
  double sin_ang_step_;
  bool   ccw_;
};

} // namespace xxg

#endif // XXG_ELLIPSE_HH
