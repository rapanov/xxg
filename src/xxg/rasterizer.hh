/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef XXG_RASTERIZER_HH
#define XXG_RASTERIZER_HH

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <xxg/fill_rule.hh>

namespace xxg
{

struct Rasterizer
{
  using Fill_rule = ::xxg::Fill_rule;

  Rasterizer() noexcept
    : x_0_(0)
    , y_0_(0)
    , x_(0)
    , y_(0)
  {}

  void move_to(const double x, const double y)
  {
    move_to_fixed_24_dot_8(to_fixed_24_dot_8(x), to_fixed_24_dot_8(y));
  }

  void line_to(const double x, const double y)
  {
    line_to_fixed_24_dot_8(to_fixed_24_dot_8(x), to_fixed_24_dot_8(y));
  }

  void move_to_fixed_24_dot_8(const int32_t x, const int32_t y)
  {
    // Close the previous contour.
    add_line(x_, y_, x_0_, y_0_);

    x_0_ = x;
    y_0_ = y;
    x_ = x;
    y_ = y;
  }

  void line_to_fixed_24_dot_8(const int32_t x, const int32_t y)
  {
    add_line(x_, y_, x, y);
    x_ = x;
    y_ = y;
  }

  void reset() noexcept
  {
    x_0_ = 0;
    y_0_ = 0;
    x_ = 0;
    y_ = 0;
    cell_rows_.clear();
    cell_buff_.clear();
  }

  template <class Span_consumer>
  void render(
    Span_consumer&& span_consumer,
    const Fill_rule fill_rule = Fill_rule::non_zero)
  {
    switch (fill_rule)
    {
    case Fill_rule::non_zero:
      render<Span_consumer, Fill_rule::non_zero>(
        static_cast<Span_consumer&&>(span_consumer));
      break;
    case Fill_rule::even_odd:
      render<Span_consumer, Fill_rule::even_odd>(
        static_cast<Span_consumer&&>(span_consumer));
      break;
    }
  }

  template <
    class Span_consumer, Fill_rule fill_rule>
  void render(Span_consumer&& span_consumer)
  {
    // Close the contour.
    add_line(x_, y_, x_0_, y_0_);
    x_ = x_0_;
    y_ = y_0_;

    const Cell_rows& cell_rows = cell_rows_;
    Cell_row* const rows = cell_rows.data_;
    const uint32_t size = cell_rows.size_;
    const uint32_t offset = cell_rows.offset_;
    const uint32_t capacity = cell_rows.capacity_;
    const uint32_t s = capacity - offset;

    static_cast<Span_consumer&&>(span_consumer).set_y(cell_rows_.min_y_);

    if (size <= s)
    {
      Cell_row* row = rows + offset;
      Cell_row* const row_end = row + size;

      for (; row < row_end; ++row)
      {
        render_row<Span_consumer, fill_rule>(
          *row, static_cast<Span_consumer&&>(span_consumer));
        static_cast<Span_consumer&&>(span_consumer).inc_y();
      }
    }
    else
    {
      Cell_row* row = rows + offset;
      Cell_row* row_end = row + s;

      for (; row < row_end; ++row)
      {
        render_row<Span_consumer, fill_rule>(
          *row, ::std::forward<Span_consumer>(span_consumer));
        static_cast<Span_consumer&&>(span_consumer).inc_y();
      }

      row = rows;
      row_end = rows + (size - s);

      for (; row < row_end; ++row)
      {
        render_row<Span_consumer, fill_rule>(
          *row, static_cast<Span_consumer&&>(span_consumer));
        static_cast<Span_consumer&&>(span_consumer).inc_y();
      }
    }
  }

private:
  struct Cell_buff;

  struct Cell
  {
    int32_t  x_;
    int32_t  cover_;
    int32_t  area_;
    uint32_t next_cell_idx_;
  };

  struct Cell_row
  {
    Cell_row() noexcept
      : size_and_order_(0u)
    {}

    uint32_t size() const noexcept
    {
      return size_and_order_ & 0x7fffffffu;
    }

    bool ordered() const noexcept
    {
      return (size_and_order_ & 0x80000000u) == 0u;
    }

    uint32_t add_cell(
      const int32_t x, const int32_t cover,
      const int32_t area, Cell_buff& cell_buff)
    {
      const uint32_t size = this->size();

      switch (size)
      {
        case 0:
        {
          const uint32_t cell_idx = cell_buff.new_cell();
          Cell& cell = cell_buff.cells_[cell_idx];
          cell.x_ = x;
          cell.cover_ = cover;
          cell.area_ = area;
          size_and_order_ = 1u;
          first_cell_idx_ = cell_idx;
          last_cell_idx_ = cell_idx;
          return cell_idx;
        }

        case 1:
        {
          Cell* existent_cell = cell_buff.cells_ + first_cell_idx_;
          const int32_t existent_x = existent_cell->x_;

          if (x == existent_x)
          {
            existent_cell->cover_ += cover;
            existent_cell->area_ += area;
            return first_cell_idx_;
          }

          const uint32_t cell_idx = cell_buff.new_cell();
          Cell& cell = cell_buff.cells_[cell_idx];
          cell.x_ = x;
          cell.cover_ = cover;
          cell.area_ = area;
          size_and_order_ = 2u;

          if (x > existent_x)
          {
            existent_cell = cell_buff.cells_ + first_cell_idx_;
            existent_cell->next_cell_idx_ = cell_idx;
            last_cell_idx_ = cell_idx;
          }
          else // (x < existentX)
          {
            cell.next_cell_idx_ = first_cell_idx_;
            first_cell_idx_ = cell_idx;
          }

          return cell_idx;
        }
      }

      Cell* last_cell = cell_buff.cells_ + last_cell_idx_;
      int32_t existent_x = last_cell->x_;

      if (x > existent_x)
      {
        const uint32_t cell_idx = cell_buff.new_cell();
        Cell& cell = cell_buff.cells_[cell_idx];
        cell.x_ = x;
        cell.cover_ = cover;
        cell.area_ = area;
        last_cell = cell_buff.cells_ + last_cell_idx_;
        last_cell->next_cell_idx_ = cell_idx;
        size_and_order_ = (size + 1u) | (size_and_order_ & 0x80000000u);
        last_cell_idx_ = cell_idx;
        return cell_idx;
      }

      if (x == existent_x)
      {
        last_cell->cover_ += cover;
        last_cell->area_ += area;

        return last_cell_idx_;
      }

      Cell* first_cell = cell_buff.cells_ + first_cell_idx_;
      existent_x = first_cell->x_;

      if (x < existent_x)
      {
        const uint32_t cell_idx = cell_buff.new_cell();
        Cell& cell = cell_buff.cells_[cell_idx];
        cell.x_ = x;
        cell.cover_ = cover;
        cell.area_ = area;

        cell.next_cell_idx_ = first_cell_idx_;
        size_and_order_ = (size + 1u) | (size_and_order_ & 0x80000000u);
        first_cell_idx_ = cell_idx;
        return cell_idx;
      }

      if (x == existent_x)
      {
        first_cell->cover_ += cover;
        first_cell->area_ += area;
        return first_cell_idx_;
      }

      switch (size)
      {
      case 2u:
        {
          const uint32_t cell_idx = cell_buff.new_cell();
          Cell& cell = cell_buff.cells_[cell_idx];
          cell.x_ = x;
          cell.cover_ = cover;
          cell.area_ = area;

          first_cell = cell_buff.cells_ + first_cell_idx_;
          first_cell->next_cell_idx_ = cell_idx;
          cell.next_cell_idx_ = last_cell_idx_;
          size_and_order_ = 3u;

          return cell_idx;
        }

      case 3u:
        {
          const uint32_t mid_cell_idx = first_cell->next_cell_idx_;
          Cell* mid_cell = cell_buff.cells_ + mid_cell_idx;
          existent_x = mid_cell->x_;

          if (x == existent_x)
          {
            mid_cell->cover_ += cover;
            mid_cell->area_ += area;

            return mid_cell_idx;
          }

          const uint32_t cell_idx = cell_buff.new_cell();
          Cell& cell = cell_buff.cells_[cell_idx];
          cell.x_ = x;
          cell.cover_ = cover;
          cell.area_ = area;
          size_and_order_ = 4;

          if (x < existent_x)
          {
            first_cell = cell_buff.cells_ + first_cell_idx_;
            first_cell->next_cell_idx_ = cell_idx;
            cell.next_cell_idx_ = mid_cell_idx;
          }
          else // (x > existentX)
          {
            mid_cell = cell_buff.cells_ + mid_cell_idx;
            mid_cell->next_cell_idx_ = cell_idx;
            cell.next_cell_idx_ = last_cell_idx_;
          }

          return cell_idx;
        }
      }

      const uint32_t cell_idx = cell_buff.new_cell();
      Cell& cell = cell_buff.cells_[cell_idx];
      cell.x_ = x;
      cell.cover_ = cover;
      cell.area_ = area;

      last_cell = cell_buff.cells_ + last_cell_idx_;
      last_cell->next_cell_idx_ = cell_idx;
      size_and_order_ = (size + 1u) | 0x80000000u; // The row is now onordered.
      last_cell_idx_ = cell_idx;

      return cell_idx;
    }

    uint32_t add_cell_to_right(
      const uint32_t cell_idx,
      const int32_t cover,
      const int32_t area,
      Cell_buff& cell_buff)
    {
      if (cell_idx == last_cell_idx_)
      {
        const uint32_t new_cell_idx = cell_buff.new_cell();
        Cell& cell = cell_buff.cells_[cell_idx];
        Cell& new_cell = cell_buff.cells_[new_cell_idx];
        new_cell.x_ = cell.x_ + 1;
        new_cell.cover_ = cover;
        new_cell.area_ = area;

        cell.next_cell_idx_ = new_cell_idx;
        size_and_order_ =
          ((size_and_order_ & 0x7fffffffu) + 1u) |
          (size_and_order_ & 0x80000000u);
        last_cell_idx_ = new_cell_idx;

        return new_cell_idx;
      }

      Cell* cell = cell_buff.cells_ + cell_idx;
      const int32_t rigth_x = cell->x_ + 1;
      const uint32_t next_cell_idx = cell->next_cell_idx_;
      Cell& next_cell = cell_buff.cells_[next_cell_idx];

      if (rigth_x == next_cell.x_)
      {
        next_cell.cover_ += cover;
        next_cell.area_ += area;

        return next_cell_idx;
      }

      const uint32_t new_cell_idx = cell_buff.new_cell();
      Cell& new_cell = cell_buff.cells_[new_cell_idx];
      new_cell.x_ = rigth_x;
      new_cell.cover_ = cover;
      new_cell.area_ = area;
      new_cell.next_cell_idx_ = next_cell_idx;

      cell = cell_buff.cells_ + cell_idx;
      cell->next_cell_idx_ = new_cell_idx;
      size_and_order_ =
        ((size_and_order_ & 0x7fffffffu) + 1u) |
        (size_and_order_ & 0x80000000u);

      return new_cell_idx;
    }

    void sort(Cell_buff& cell_buff)
    {
      // - Dump cell indices from this row to an array -
      //   a contiguous space of memory.
      // - Make binary heap out of this array. This will be a min-heap
      //   by 'x' coordinate of the corresponding cells.

      // We gonna use space at the tail of the cell buffer - 'cellBuff'.
      uint32_t size = this->size();
      const size_t required_space = size_t(size) * sizeof(uint32_t);
      const size_t available_space =
        size_t(cell_buff.size_ - cell_buff.next_idx_) * sizeof(Cell);

      if (required_space > available_space)
      {
        const size_t increase = required_space - available_space; // In bytes.
        const uint32_t increase_in_cells =
          static_cast<uint32_t>((increase + sizeof(Cell) - 1u) / sizeof(Cell));
        cell_buff.inflate(increase_in_cells);
      }

      Cell* const cells = cell_buff.cells_;
      uint32_t* const indices =
        reinterpret_cast<uint32_t*>(cell_buff.cells_ + cell_buff.next_idx_);
      uint32_t cell_idx = first_cell_idx_;
      Cell* cell = cells + cell_idx;
      uint32_t* idx_ptr = indices;
      uint32_t* const idx_ptr_end = idx_ptr + size;
      *idx_ptr = cell_idx;

      for (++idx_ptr; idx_ptr < idx_ptr_end; ++idx_ptr)
      {
        cell_idx = cell->next_cell_idx_;
        cell = cells + cell_idx;
        *idx_ptr = cell_idx;
      }

      uint32_t j = (size >> 1u) - 1u;
      uint32_t i, left_child_i, right_cild_i, tmp;
      int32_t x, left_child_x, right_child_x;

      for (; ; --j)
      {
        for (i = j; ; )
        {
          left_child_i = (i << 1) + 1;

          if (left_child_i >= size)
          {
            break;
          }

          right_cild_i = left_child_i + 1;

          uint32_t& idx = indices[i];
          uint32_t& left_child_idx = indices[left_child_i];

          x = cells[idx].x_;
          left_child_x = cells[left_child_idx].x_;

          if (right_cild_i >= size)
          {
            if (x > left_child_x)
            {
              tmp = idx;
              idx = left_child_idx;
              left_child_idx = tmp;
            }

            break;
          }

          uint32_t& right_child_idx = indices[right_cild_i];
          right_child_x = cells[right_child_idx].x_;

          if (x > left_child_x)
          {
            if (left_child_x < right_child_x)
            {
              tmp = idx;
              idx = left_child_idx;
              left_child_idx = tmp;
              i = left_child_i;
            }
            else
            {
              tmp = idx;
              idx = right_child_idx;
              right_child_idx = tmp;
              i = right_cild_i;
            }
          }
          else if (x > right_child_x)
          {
            tmp = idx;
            idx = right_child_idx;
            right_child_idx = tmp;
            i = right_cild_i;
          }
          else
          {
            break;
          }
        }

        if (j == 0)
        {
          break;
        }
      }

      first_cell_idx_ = *indices;
      cell = cells + *indices;

      for (--size; size; --size)
      {
        *indices = indices[size];

        for (i = 0; ; )
        {
          left_child_i = (i << 1) + 1;

          if (left_child_i >= size)
          {
            break;
          }

          right_cild_i = left_child_i + 1;

          uint32_t& idx = indices[i];
          uint32_t& left_child_idx = indices[left_child_i];

          x = cells[idx].x_;
          left_child_x = cells[left_child_idx].x_;

          if (right_cild_i >= size)
          {
            if (x > left_child_x)
            {
              tmp = idx;
              idx = left_child_idx;
              left_child_idx = tmp;
            }

            break;
          }

          uint32_t& right_child_idx = indices[right_cild_i];
          right_child_x = cells[right_child_idx].x_;

          if (x > left_child_x)
          {
            if (left_child_x < right_child_x)
            {
              tmp = idx;
              idx = left_child_idx;
              left_child_idx = tmp;
              i = left_child_i;
            }
            else
            {
              tmp = idx;
              idx = right_child_idx;
              right_child_idx = tmp;
              i = right_cild_i;
            }
          }
          else if (x > right_child_x)
          {
            tmp = idx;
            idx = right_child_idx;
            right_child_idx = tmp;
            i = right_cild_i;
          }
          else
          {
            break;
          }
        }

        cell->next_cell_idx_ = *indices;
        cell = cells + *indices;
      }

      // Clear the highest bit as the row is now ordered.
      size_and_order_ &= 0x7fffffff;
    }

    // Bit 31 contains ordering information:
    // 0 - Ordered by x in ascending order.
    // 1 - Unordered, or order is unknown.
    uint32_t size_and_order_;
    uint32_t first_cell_idx_;
    uint32_t last_cell_idx_;
  };

  struct Cell_rows
  {
    Cell_rows() noexcept
      : data_(nullptr)
      , offset_(0)
      , size_(0)
      , capacity_(0)
      , min_y_(0)
      , max_y_(-1)
    {}

    Cell_rows(const Cell_rows&) = delete;

    ~Cell_rows()
    {
      if (data_ != nullptr)
      {
        ::std::free(data_);
      }
    }

    Cell_rows& operator = (const Cell_rows&) = delete;

    void clear() noexcept
    {
      offset_ = 0;
      size_ = 0;
      min_y_ = 0;
      max_y_ = -1;
    }

    Cell_row& find(int32_t y)
    {
      const uint32_t size = size_;

      if (size == 0)
      {
        if (capacity_ == 0)
        {
          static const uint32_t initial_capacity = 16u;
          data_ = static_cast<Cell_row*>(::std::malloc(
            static_cast<size_t>(initial_capacity) * sizeof(Cell_row)));
          capacity_ = initial_capacity;
        }

        offset_ = 0;
        size_ = 1;
        min_y_ = y;
        max_y_ = y;
        data_->size_and_order_ = 0;

        return *data_;
      }

      const uint32_t offset = offset_;
      const uint32_t capacity = capacity_;
      const int32_t min_y = min_y_;
      const int32_t max_y = max_y_;

      if (y >= min_y && y <= max_y)
      {
        const uint32_t row_idx =
          static_cast<uint32_t>(y) - static_cast<uint32_t>(min_y);
        const uint32_t s = capacity - offset;

        if (row_idx < s)
        {
          return data_[offset + row_idx];
        }

        return data_[row_idx - s];
      }

      uint32_t new_size;
      Cell_row* row;

      if (y > max_y)
      {
        new_size = static_cast<uint32_t>(y) - static_cast<uint32_t>(min_y) + 1u;
        const uint32_t size_increase = new_size - size;

        if (new_size <= capacity)
        {
          uint32_t end_offset;
          uint32_t new_end_offset;
          const uint32_t s = capacity - offset;

          if (new_size <= s)
          {
            end_offset = offset + size;
            new_end_offset = offset + new_size;
          }
          else if (size <= s)
          {
            end_offset = offset + size;
            new_end_offset = new_size - s;
          }
          else
          {
            end_offset = size - s;
            new_end_offset = new_size - s;
          }

          if (new_end_offset > end_offset)
          {
            row = construct(data_ + end_offset, size_increase);
          }
          else
          {
            construct(data_ + end_offset, capacity - end_offset);
            row = construct(data_, new_end_offset);
          }
        }
        else
        {
          // Capacity increase (i.e. reallocation) is needed.
          uint32_t new_capacity =
            capacity < 0x80000000u ? capacity << 1 : 0xffffffffu;
          Cell_row* new_data;

          if (new_capacity < new_size)
          {
            new_capacity = new_size;
          }

          if (offset == 0)
          {
            new_data = static_cast<Cell_row*>(::std::realloc(
              data_, static_cast<size_t>(new_capacity) * sizeof(Cell_row)));
            row = construct(new_data + size, size_increase);
          }
          else
          {
            new_data = static_cast<Cell_row*>(::std::malloc(
              static_cast<size_t>(new_capacity) * sizeof(Cell_row)));
            const uint32_t s = capacity - offset;

            if (size <= s)
            {
              ::std::memcpy(
                new_data, data_ + offset,
                static_cast<size_t>(size) * sizeof(Cell_row));
            }
            else
            {
              ::std::memcpy(
                new_data, data_ + offset,
                static_cast<size_t>(s) * sizeof(Cell_row));
              ::std::memcpy(
                new_data + s, data_,
                static_cast<size_t>(size - s) * sizeof(Cell_row));
            }

            ::std::free(data_);
          }

          row = construct(new_data + size, size_increase);
          data_ = new_data;
          offset_ = 0;
          capacity_ = new_capacity;
        }

        max_y_ = y;
      }
      else // (y < minY)
      {
        new_size = static_cast<uint32_t>(max_y) - static_cast<uint32_t>(y) + 1u;
        const uint32_t size_increase = new_size - size;

        if (new_size <= capacity)
        {
          uint32_t new_offset;

          if (size_increase <= offset)
          {
            new_offset = offset - size_increase;
            row = data_ + new_offset;
            construct(row, size_increase);
          }
          else
          {
            const uint32_t a = size_increase - offset;
            new_offset = capacity - a;
            construct(data_, offset);
            row = data_ + new_offset;
            construct(row, a);
          }

          offset_ = new_offset;
        }
        else
        {
          // Capacity increase (i.e. reallocation) is needed.
          uint32_t new_capacity =
            capacity < 0x80000000u ? capacity << 1 : 0xffffffffu;

          if (new_capacity < new_size)
          {
            new_capacity = new_size;
          }

          Cell_row* const new_data = static_cast<Cell_row*>(
            ::std::malloc(static_cast<size_t>(new_capacity) * sizeof(Cell_row)));
          const uint32_t s = capacity - offset;

          construct(new_data, size_increase);
          Cell_row* dst = new_data + size_increase;

          if (size <= s)
          {
            ::std::memcpy(
              dst, data_ + offset,
              static_cast<size_t>(size) * sizeof(Cell_row));
          }
          else
          {
            ::std::memcpy(
              dst, data_ + offset,
              static_cast<size_t>(s) * sizeof(Cell_row));
            dst += s;
            ::std::memcpy(
              dst, data_, static_cast<size_t>(size - s) * sizeof(Cell_row));
          }

          ::std::free(data_);

          row = new_data;
          data_ = new_data;
          offset_ = 0;
          capacity_ = new_capacity;
        }

        min_y_ = y;
      }

      size_ = new_size;
      return *row;
    }

    static Cell_row* construct(Cell_row* rows, const uint32_t count) noexcept
    {
      if (count > 0u)
      {
        Cell_row* const lastRow = rows + count - 1u;

        for (; rows <= lastRow; ++rows)
        {
          rows->size_and_order_ = 0u;
        }

        return lastRow;
      }

      return nullptr;
    }

    Cell_row* data_;
    uint32_t  offset_;
    uint32_t  size_;
    uint32_t  capacity_;
    int32_t   min_y_;
    int32_t   max_y_;
  };

  struct Cell_buff
  {
    Cell_buff() noexcept
      : cells_(nullptr)
      , size_(0)
      , next_idx_(0)
    {}

    Cell_buff(const Cell_buff&) = delete;

    ~Cell_buff()
    {
      if (cells_ != nullptr)
      {
        ::std::free(cells_);
      }
    }

    Cell_buff& operator = (const Cell_buff&) = delete;

    void clear() noexcept
    {
      next_idx_ = 0;
    }

    uint32_t new_cell()
    {
      uint32_t idx = next_idx_;

      if (idx >= size_)
      {
        if (size_ > 0)
        {
          const uint32_t new_size = size_ << 1u;
          cells_ = static_cast<Cell*>(::std::realloc(
            cells_, static_cast<size_t>(new_size) * sizeof(Cell)));
          size_ = new_size;
        }
        else
        {
          static const uint32_t initial_size = 16u;
          cells_ = static_cast<Cell*>(::std::malloc(
            static_cast<size_t>(initial_size) * sizeof(Cell)));
          size_ = initial_size;
        }
      }

      ++next_idx_;
      return idx;
    }

    void inflate(const uint32_t increase)
    {
      const uint32_t size = size_;
      const uint32_t new_size = size + increase;

      if (size > 0)
      {
        if (size == next_idx_)
        {
          cells_ = static_cast<Cell*>(::std::realloc(
            cells_, static_cast<size_t>(new_size) * sizeof(Cell)));
        }
        else
        {
          Cell* const new_cells = static_cast<Cell*>(
            ::std::malloc(static_cast<size_t>(new_size) * sizeof(Cell)));
          ::std::memcpy(
            new_cells, cells_, static_cast<size_t>(next_idx_) * sizeof(Cell));
          ::std::free(cells_);
          cells_ = new_cells;
        }
      }
      else
      {
        cells_ = static_cast<Cell*>(
          ::std::malloc(static_cast<size_t>(new_size) * sizeof(Cell)));
      }

      size_ = new_size;
    }

    Cell*    cells_;
    uint32_t size_;
    uint32_t next_idx_;
  };

  enum class Direction
  {
      positive,
      negative
  };

  void add_line(
    const int32_t x_0, const int32_t y_0, const int32_t x_1, const int32_t y_1)
  {
    if (y_0 == y_1)
    {
      return;
    }

    Cell_rows& rows = cell_rows_;
    Cell_buff& cell_buff = cell_buff_;

    if (x_0 == x_1)
    {
      // Vertical line.
      const int32_t int_x = x_0 >> 8;
      const int32_t frac_x = x_0 & 0xff;
      int32_t int_y_0 = y_0 >> 8;
      int32_t int_y_1 = y_1 >> 8;
      const int32_t frac_y_0 = y_0 & 0xff;
      const int32_t frac_y_1 = y_1 & 0xff;
      int32_t cover;
      int32_t area;

      if (int_y_0 == int_y_1)
      {
        cover = frac_y_1 - frac_y_0;
        area = (cover * frac_x) << 1;
        Cell_row& row = rows.find(int_y_0);
        row.add_cell(int_x, cover, area, cell_buff);
        return;
      }

      if (y_0 < y_1)
      {
        if (frac_y_0)
        {
          cover = 0x100 - frac_y_0;
          area = (cover * frac_x) << 1;
          Cell_row& row = rows.find(int_y_0);
          row.add_cell(int_x, cover, area, cell_buff);
          ++int_y_0;
        }

        if (frac_y_1)
        {
          cover = frac_y_1;
          area = (cover * frac_x) << 1;
          Cell_row& row = rows.find(int_y_1);
          row.add_cell(int_x, cover, area, cell_buff);
        }

        cover = 0x100;
        area = frac_x << 9; // (cover * fracX) * 2
      }
      else
      {
        if (frac_y_0)
        {
          cover = -frac_y_0;
          area = (cover * frac_x) << 1;
          Cell_row& row = rows.find(int_y_0);
          row.add_cell(int_x, cover, area, cell_buff);
        }

        if (frac_y_1)
        {
          cover = frac_y_1 - 0x100;
          area = (cover * frac_x) << 1;
          Cell_row& row = rows.find(int_y_1);
          row.add_cell(int_x, cover, area, cell_buff);
          ++int_y_1;
        }

        const int32_t tmp = int_y_0;
        int_y_0 = int_y_1;
        int_y_1 = tmp;

        cover = -0x100;
        area = -(frac_x << 9); // (cover * fracX) * 2
      }

      while (int_y_0 < int_y_1)
      {
        Cell_row& row = rows.find(int_y_0);
        row.add_cell(int_x, cover, area, cell_buff);
        ++int_y_0;
      }

      return;
    }

    if (x_1 > x_0)
    {
      if (y_1 > y_0)
      {
        add_line<
          Direction::positive,
          Direction::positive,
          Direction::positive>(x_0, y_0, x_1, y_1);
      }
      else
      {
        add_line<
          Direction::positive,
          Direction::negative,
          Direction::negative>(x_1, y_1, x_0, y_0);
      }
    }
    else
    {
      if (y_1 > y_0)
      {
        add_line<
          Direction::negative,
          Direction::positive,
          Direction::negative>(x_0, y_0, x_1, y_1);
      }
      else
      {
        add_line<
          Direction::negative,
          Direction::negative,
          Direction::positive>(x_1, y_1, x_0, y_0);
      }
    }
  }

  template <
    Direction x_direction,
    Direction y_direction,
    Direction x_y_direction>
  void add_line(int32_t x_0, int32_t y_0, int32_t x_1, int32_t y_1)
  {
    int32_t int_x_0 = x_0 >> 8;
    int32_t int_x_1 = x_1 >> 8;
    int32_t int_y_0 = y_0 >> 8;
    int32_t int_y_1 = y_1 >> 8;
    int32_t frac_x_0 = x_0 & 0xff;
    int32_t frac_x_1 = x_1 & 0xff;

    if (int_y_0 == int_y_1)
    {
      // Only one scanline is involved.

      if constexpr (x_y_direction == Direction::positive)
      {
        add_scanline<y_direction>(
          int_y_0, int_x_0, int_x_1, frac_x_0, frac_x_1, x_1 - x_0, y_1 - y_0);
      }
      else
      {
        add_scanline<y_direction>(
          int_y_0, int_x_1, int_x_0, frac_x_1, frac_x_0, x_0 - x_1, y_1 - y_0);
      }

      return;
    }

    uint32_t d_x;
    uint32_t d_y = static_cast<uint32_t>(y_1 - y_0);
    int32_t frac_y_0 = y_0 & 0xff;
    int32_t frac_y_1 = y_1 & 0xff;

    if constexpr (x_y_direction == Direction::positive)
    {
      d_x = static_cast<uint32_t>(x_1 - x_0);
    }
    else
    {
      d_x = static_cast<uint32_t>(x_0 - x_1);
    }

    int32_t int_y = int_y_0;
    int32_t x = x_0;
    int32_t int_x, frac_x;
    int32_t delta_x;
    uint32_t rem;

    if (frac_y_0)
    {
      const int32_t delta_y = 0x100 - frac_y_0;

      if (d_x < 0x1000000u)
      {
        const uint32_t p = d_x * static_cast<uint32_t>(delta_y);
        delta_x = static_cast<int32_t>(p / d_y);
        rem = p % d_y;
      }
      else
      {
        const uint64_t p =
          static_cast<uint64_t>(d_x) * static_cast<uint32_t>(delta_y);
        delta_x = static_cast<int32_t>(p / d_y);
        rem = static_cast<uint32_t>(p % d_y);
      }

      if constexpr (x_direction == Direction::negative)
      {
        if (rem)
        {
          ++delta_x;
          rem = d_y - rem;
        }
      }

      if constexpr (x_y_direction == Direction::positive)
      {
        x += delta_x;
      }
      else
      {
        x -= delta_x;
      }

      int_x = x >> 8;
      frac_x = x & 0xff;

      if constexpr (x_y_direction == Direction::positive)
      {
        add_scanline<y_direction>(
          int_y_0, int_x_0, int_x, frac_x_0, frac_x, x - x_0, delta_y);
      }
      else
      {
        add_scanline<y_direction>(
          int_y_0, int_x, int_x_0, frac_x, frac_x_0, x_0 - x, delta_y);
      }

      ++int_y;
    }
    else
    {
      int_x = int_x_0;
      frac_x = frac_x_0;
      rem = 0;
    }

    if (int_y < int_y_1)
    {
      int32_t inc_x;
      uint32_t mod;
      int32_t annex;

      if (d_x < 0x1000000u)
      {
        const uint32_t p = d_x << 8;
        inc_x = static_cast<int32_t>(p / d_y);
        mod = p % d_y;
      }
      else
      {
        const uint64_t p = static_cast<uint64_t>(d_x) << 8;
        inc_x = static_cast<int32_t>(p / d_y);
        mod = p % d_y;
      }

      if constexpr (x_direction == Direction::positive)
      {
        annex = 1;
      }
      else
      {
        if (mod)
        {
          ++inc_x;
          mod = d_y - mod;
        }

        annex = -1;
      }

      Cell_rows& rows = cell_rows_;
      Cell_buff& cell_buff = cell_buff_;
      int32_t cover, area;

      do
      {
        delta_x = inc_x;
        rem += mod;

        if (rem >= d_y)
        {
          delta_x += annex;
          rem -= d_y;
        }

        Cell_row& row = rows.find(int_y);
        int32_t next_x, int_next_x, frac_next_x;
        int32_t left_x, right_x;
        int32_t int_left_x, int_right_x;
        int32_t frac_left_x, frac_right_x;

        if constexpr (x_y_direction == Direction::positive)
        {
          next_x = x + delta_x;
          int_next_x = next_x >> 8;
          frac_next_x = next_x & 0xff;
          left_x = x;
          right_x = next_x;
          int_left_x = int_x;
          int_right_x = int_next_x;
          frac_left_x = frac_x;
          frac_right_x = frac_next_x;
        }
        else
        {
          next_x = x - delta_x;
          int_next_x = next_x >> 8;
          frac_next_x = next_x & 0xff;
          left_x = next_x;
          right_x = x;
          int_left_x = int_next_x;
          int_right_x = int_x;
          frac_left_x = frac_next_x;
          frac_right_x = frac_x;
        }

        if (int_left_x == int_right_x)
        {
          // Inside one cell.

          if constexpr (y_direction == Direction::positive)
          {
            cover = 0x100;
            area = (frac_left_x + frac_right_x) << 8;
          }
          else
          {
            cover = -0x100;
            area = -((frac_left_x + frac_right_x) << 8);
          }

          row.add_cell(int_left_x, cover, area, cell_buff);
        }
        else
        {
          int_x = int_left_x;
          int32_t y = 0;
          int32_t delta_y;
          int32_t rem_1;
          uint32_t cell_idx;
          bool cell_idx_valid;

          if (frac_left_x)
          {
            const int32_t p_1 = (0x100 - frac_left_x) << 8;
            delta_y = p_1 / delta_x;
            rem_1 = p_1 % delta_x;

            if constexpr (y_direction == Direction::positive)
            {
              cover = delta_y;
            }
            else
            {
              if (rem_1)
              {
                ++delta_y;
                rem_1 = delta_x - rem_1;
              }

              cover = -delta_y;
            }

            area = cover * (frac_left_x + 0x100);
            cell_idx = row.add_cell(int_left_x, cover, area, cell_buff);
            ++int_x;
            y += delta_y;
            cell_idx_valid = true;
          }
          else
          {
            rem_1 = 0;
            cell_idx_valid = false;
          }

          if (int_x < int_right_x)
          {
            int32_t inc_y = 0x10000 / delta_x;
            int32_t mod_1 = 0x10000 % delta_x;
            int32_t annex_1;

            if constexpr (y_direction == Direction::positive)
            {
              annex_1 = 1;
            }
            else
            {
              if (mod_1)
              {
                ++inc_y;
                mod_1 = delta_x - mod_1;
              }

              annex_1 = -1;
            }

            do
            {
              delta_y = inc_y;
              rem_1 += mod_1;

              if (rem_1 >= delta_x)
              {
                delta_y += annex_1;
                rem_1 -= delta_x;
              }

              if constexpr (y_direction == Direction::positive)
              {
                cover = delta_y;
              }
              else
              {
                cover = -delta_y;
              }

              area = cover << 8;

              if (cell_idx_valid)
              {
                cell_idx = row.add_cell_to_right(cell_idx, cover, area, cell_buff);
              }
              else
              {
                cell_idx = row.add_cell(int_x, cover, area, cell_buff);
                cell_idx_valid = true;
              }

              y += delta_y;
              ++int_x;
            }
            while (int_x < int_right_x);
          }

          if (frac_right_x)
          {
            delta_y = 0x100 - y;

            if (delta_y)
            {
              if constexpr (y_direction == Direction::positive)
              {
                cover = delta_y;
              }
              else
              {
                cover = -delta_y;
              }

              area = cover * frac_right_x;
              row.add_cell_to_right(cell_idx, cover, area, cell_buff);
            }
          }
        }

        x = next_x;
        int_x = int_next_x;
        frac_x = frac_next_x;
        ++int_y;
      }
      while (int_y < int_y_1);
    }

    if (frac_y_1)
    {
      if constexpr (x_y_direction == Direction::positive)
      {
        add_scanline<y_direction>(
          int_y_1, int_x, int_x_1, frac_x, frac_x_1, x_1 - x, frac_y_1);
      }
      else
      {
        add_scanline<y_direction>(
          int_y_1, int_x_1, int_x, frac_x_1, frac_x, x - x_1, frac_y_1);
      }
    }
  }

  template <Direction y_direction>
  void add_scanline(
    const int32_t int_y,
    const int32_t int_x_0, const int32_t int_x_1,
    const int32_t frac_x_0, const int32_t frac_x_1,
    const int32_t d_x, const int32_t d_y)
  {
    Cell_row& row = cell_rows_.find(int_y);

    if (int_x_0 == int_x_1)
    {
      // Inside one cell.
      int32_t cover;

      if constexpr (y_direction == Direction::positive)
      {
        cover = d_y;
      }
      else
      {
        cover = -d_y;
      }

      const int32_t area = cover * (frac_x_0 + frac_x_1);
      row.add_cell(int_x_0, cover, area, cell_buff_);
      return;
    }

    int32_t int_x = int_x_0;
    int32_t y = 0;
    int32_t delta_y;
    int32_t cover, area;
    int32_t p, rem;
    uint32_t cell_idx;
    bool cell_idx_valid;

    if (frac_x_0)
    {
      p = (0x100 - frac_x_0) * d_y;
      delta_y = p / d_x;
      rem = p % d_x;

      if constexpr (y_direction == Direction::positive)
      {
        cover = delta_y;
      }
      else
      {
        if (rem)
        {
          ++delta_y;
          rem = d_x - rem;
        }

        cover = -delta_y;
      }

      area = cover * (frac_x_0 + 0x100);
      cell_idx = row.add_cell(int_x_0, cover, area, cell_buff_);
      ++int_x;
      y += delta_y;
      cell_idx_valid = true;
    }
    else
    {
      rem = 0;
      cell_idx_valid = false;
    }

    if (int_x < int_x_1)
    {
      p = d_y << 8;
      int32_t inc_y = p / d_x;
      int32_t mod = p % d_x;
      int32_t annex;

      if constexpr (y_direction == Direction::positive)
      {
        annex = 1;
      }
      else
      {
        if (mod)
        {
          ++inc_y;
          mod = d_x - mod;
        }

        annex = -1;
      }

      do
      {
        delta_y = inc_y;
        rem += mod;

        if (rem >= d_x)
        {
          delta_y += annex;
          rem -= d_x;
        }

        if constexpr (y_direction == Direction::positive)
        {
          cover = delta_y;
        }
        else
        {
          cover = -delta_y;
        }

        area = cover << 8;

        if (cell_idx_valid)
        {
          cell_idx = row.add_cell_to_right(cell_idx, cover, area, cell_buff_);
        }
        else
        {
          cell_idx = row.add_cell(int_x, cover, area, cell_buff_);
          cell_idx_valid = true;
        }

        y += delta_y;
        ++int_x;
      }
      while (int_x < int_x_1);
    }

    if (frac_x_1)
    {
      delta_y = d_y - y;

      if (delta_y)
      {
        if constexpr (y_direction == Direction::positive)
        {
          cover = delta_y;
        }
        else
        {
          cover = -delta_y;
        }

        area = cover * frac_x_1;
        row.add_cell_to_right(cell_idx, cover, area, cell_buff_);
      }
    }
  }

  template <class Span_consumer, Fill_rule fill_rule>
  void render_row(Cell_row& row, Span_consumer&& span_consumer)
  {
    const uint32_t size = row.size();

    if (size == 0)
    {
      return;
    }

    if (!row.ordered())
    {
      row.sort(cell_buff_);
    }

    const Cell* cells = cell_buff_.cells_;
    const Cell* cell = cells + row.first_cell_idx_;
    int32_t cell_x = cell->x_;
    int32_t cover = cell->cover_;
    int32_t area = cell->area_;
    int32_t span_x;
    uint32_t span_len;
    uint8_t span_coverage;
    bool has_pending_span = false;

    for (uint32_t i = 1u; ; )
    {
      if (i < size)
      {
        cell = cells + cell->next_cell_idx_;
        int32_t next_cell_x = cell->x_;

        if (next_cell_x == cell_x)
        {
          // Sum up 'cover' and 'area' of all cells with the same 'x'.
          cover += cell->cover_;
          area += cell->area_;
        }
        else
        {
          const uint8_t coverage =
            compute_cell_coverage<fill_rule>(cover, area);

          // There number of cells between the cell with coordinate 'cellX'
          // and the cell with coordinate 'nextCellX', so called mid cells.
          const int32_t mid_cell_count = next_cell_x - cell_x - 1;

          if (mid_cell_count)
          {
            int32_t mid_cover = cover >= 0 ? cover : -cover;

            if constexpr (fill_rule == Fill_rule::non_zero)
            {
              if (mid_cover > 0x100)
              {
                mid_cover = 0x100;
              }
            }
            else // (fill_rule == Fill_rule::even_odd)
            {
              if (((mid_cover >> 8) & 1) == 0)
              {
                // Even.
                mid_cover &= 0xff;
              }
              else
              {
                // Odd.
                mid_cover = 0x100 - (mid_cover & 0xff);
              }
            }

            // mid_coverage = mid_cover * 255 / 256.
            const uint8_t mid_coverage =
              static_cast<uint8_t>(((mid_cover << 8) - mid_cover) >> 8);

            if (mid_coverage)
            {
              // The mid cells are to be covered.

              if (has_pending_span)
              {
                if (coverage == span_coverage)
                {
                  // Prolong the pending span;
                  ++span_len;
                }
                else
                {
                  static_cast<Span_consumer&&>(span_consumer).add_span(
                    span_x, span_len, span_coverage);
                  span_x = cell_x;
                  span_len = 1;
                  span_coverage = coverage;
                }
              }
              else
              {
                span_x = cell_x;
                span_len = 1;
                span_coverage = coverage;
                has_pending_span = true;
              }

              if (mid_coverage == span_coverage)
              {
                // Prolong the pending span;
                span_len += mid_cell_count;
              }
              else
              {
                static_cast<Span_consumer&&>(span_consumer).add_span(
                  span_x, span_len, span_coverage);
                span_x = cell_x + 1;
                span_len = mid_cell_count;
                span_coverage = mid_coverage;
              }
            }
            else
            {
              if (has_pending_span)
              {
                if (coverage == span_coverage)
                {
                  // Append this cell to the pending span.
                  // We can do it as the cell has the same coverage.
                  ++span_len;
                  static_cast<Span_consumer&&>(span_consumer).add_span(
                    span_x, span_len, span_coverage);
                }
                else
                {
                  static_cast<Span_consumer&&>(span_consumer).add_span(
                    span_x, span_len, span_coverage);

                  if (coverage)
                  {
                    static_cast<Span_consumer&&>(span_consumer).add_span(
                      cell_x, 1, coverage);
                  }
                }

                has_pending_span = false;
              }
              else
              {
                if (coverage)
                {
                  static_cast<Span_consumer&&>(span_consumer).add_span(
                    cell_x, 1, coverage);
                }
              }
            }
          }
          else
          {
            // Adjacent cells.

            if (has_pending_span)
            {
              if (coverage == span_coverage)
              {
                // Prolong the pending span;
                ++span_len;
              }
              else
              {
                static_cast<Span_consumer&&>(span_consumer).add_span(
                  span_x, span_len, span_coverage);

                if (coverage)
                {
                  span_x = cell_x;
                  span_len = 1;
                  span_coverage = coverage;
                }
                else
                {
                  has_pending_span = false;
                }
              }
            }
            else
            {
              if (coverage)
              {
                span_x = cell_x;
                span_len = 1;
                span_coverage = coverage;
                has_pending_span = true;
              }
            }
          }

          cell_x = next_cell_x;
          cover += cell->cover_;
          area = cell->area_;
        }

        ++i;
      }
      else // (i == size)
      {
        const uint8_t coverage = compute_cell_coverage<fill_rule>(cover, area);

        if (has_pending_span)
        {
          if (span_coverage == coverage)
          {
            // Append this cell to the pending span.
            // We can do it as the cell has the same coverage.
            ++span_len;
            static_cast<Span_consumer&&>(span_consumer).add_span(
              span_x, span_len, span_coverage);
          }
          else
          {
            // Add the pending span.
            static_cast<Span_consumer&&>(span_consumer).add_span(
              span_x, span_len, span_coverage);

            if (coverage)
            {
              // And the last one that consists of only one cell.
              static_cast<Span_consumer&&>(span_consumer).add_span(
                cell_x, 1, coverage);
            }
          }
        }
        else
        {
          if (coverage)
          {
            // There is no pending span.
            // Just add the span consisting of only one cell.
            static_cast<Span_consumer&&>(span_consumer).add_span(
              cell_x, 1u, coverage);
          }
        }

        break;
      }
    }
  }

  template <Fill_rule fill_rule>
  static uint8_t compute_cell_coverage(
    const int32_t cover, const int32_t area) noexcept
  {
    int32_t c = (cover << 9) - area;

    if (c < 0)
    {
      c = -c;
    }

    if constexpr (fill_rule == Fill_rule::non_zero)
    {
      if (c > 0x20000)
      {
        c = 0x20000;
      }
    }
    else // (fill_rule == Fill_rule::eben_odd)
    {
      if (((c >> 17) & 1) == 0)
      {
        // Even.
        c &= 0x1ffff;
      }
      else
      {
        // Odd.
        c = 0x20000 - (c & 0x1ffff);
      }
    }

    c >>= 9;
    c = ((c << 8) - c + 0x80) >> 8; // floor(c * 255 / 256 + 0.5)
    return static_cast<uint8_t>(c);
  }

  static int32_t to_fixed_24_dot_8(double x)
  {
    x *= 256.0;
    x += x >= 0.0 ? 0.5 : -0.5;
    return static_cast<int32_t>(x);
  }

  int32_t x_0_;
  int32_t y_0_;
  int32_t x_;
  int32_t y_;

  Cell_rows cell_rows_;
  Cell_buff cell_buff_;
};

} // namespace xxg

#endif // XXG_RASTERIZER_HH
