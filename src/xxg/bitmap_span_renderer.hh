/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef XXG_BITMAPSPANRENDERER_HH
#define XXG_BITMAPSPANRENDERER_HH

#include <cstddef>
#include <cstdint>

namespace xxg
{

template <class Pixel, class Blender>
struct Bitmap_span_renderer
{
  explicit Bitmap_span_renderer(
    Pixel* const begin,
    Blender&& blender,
    const uint32_t width,
    const uint32_t height,
    const size_t scanline_size) noexcept
    : begin_(begin)
    , current_line_(begin)
    , blender_(blender)
    , width_(width)
    , height_(height)
    , scanline_size_(scanline_size)
    , current_y_(0)
    , color_()
  {}

  const Pixel& color() const noexcept
  {
    return color_;
  }

  void set_color(const Pixel& color)
  {
    color_ = color;
  }

  void set_y(const int32_t y) noexcept
  {
    current_y_ = y;

    if (is_y_in_range())
    {
      compute_current_line();
    }
    else
    {
      current_line_ = nullptr;
    }
  }

  void inc_y() noexcept
  {
    ++current_y_;

    if (is_y_in_range())
    {
      if (current_line_)
      {
        current_line_ = reinterpret_cast<Pixel*>(
          reinterpret_cast<char*>(current_line_) + scanline_size_);
      }
      else
      {
        compute_current_line();
      }
    }
    else
    {
      current_line_ = nullptr;
    }
  }

  void add_span(int32_t span_x, uint32_t span_len, const uint8_t coverage)
  {
    if (!current_line_)
    {
      return;
    }

    uint32_t span_right;

    if (span_x >= 0)
    {
      span_right = static_cast<uint32_t>(span_x) + span_len;
    }
    else
    {
      // To be clipped to the left.
      const uint32_t left_cut = static_cast<uint32_t>(-span_x);

      if (left_cut >= span_len)
      {
        return;
      }

      span_x = 0;
      span_len -= left_cut;
      span_right = span_len;
    }

    if (span_right > width_)
    {
      // To be clipped to the right;
      const uint32_t right_cut = span_right - width_;

      if (right_cut >= span_len)
      {
        return;
      }

      span_len -= right_cut;
    }

    Pixel* p = current_line_ + span_x;
    Pixel* const p_end = p + span_len;

    while (p < p_end)
    {
      static_cast<Blender&&>(blender_)(color_, *p, coverage);
      ++p;
    }
  }

private:
  void compute_current_line() noexcept
  {
    current_line_ = reinterpret_cast<Pixel*>(
      reinterpret_cast<char*>(begin_) +
      scanline_size_ * static_cast<size_t>(current_y_));
  }

  bool is_y_in_range() const noexcept
  {
    return current_y_ >= 0 && static_cast<uint32_t>(current_y_) < width_;
  }

  Pixel*    begin_;
  Pixel*    current_line_;
  Blender&& blender_;
  uint32_t  width_;
  uint32_t  height_;
  size_t    scanline_size_;
  int32_t   current_y_;
  Pixel     color_;
};

} // namespace xxg

#endif // XXG_BITMAPSPANRENDERER_HH
