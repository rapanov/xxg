/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef XXG_RGB888BLENDER_HH
#define XXG_RGB888BLENDER_HH

#include <cstdint>

namespace xxg
{

struct Rgb_888_blender
{
  void operator () (const uint32_t src, uint32_t& dst, uint8_t cover) noexcept
  {
    if (cover == 0xffu)
    {
      dst = src;
    }
    else
    {
      const int32_t c = static_cast<int32_t>(cover);
      const uint32_t r = blend(
        static_cast<int32_t>((src >> 16u) & 0xff),
        static_cast<int32_t>((dst >> 16u) & 0xff), c);
      const uint32_t g = blend(
        static_cast<int32_t>((src >> 8u) & 0xff),
        static_cast<int32_t>((dst >> 8u) & 0xff), c);
      const uint32_t b = blend(
        static_cast<int32_t>(src & 0xff), static_cast<int32_t>(dst & 0xff), c);
      dst = (r << 16u) | (g << 8u) | b;
    }
  }

private:
  static uint32_t blend(int32_t src, int32_t dst, int32_t cover) noexcept
  {
    // result = (src * cover + dst * (255 - cover)) / 255
    int32_t val = (dst << 8) - dst + cover * (src - dst);
    val = (val + 1 + (val >> 8)) >> 8; // val = val / 255
    return static_cast<uint32_t>(val);
  }
};

} // namespace xxg

#endif // XXG_RGB888BLENDER_HH
