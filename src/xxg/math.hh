#ifndef XXG_MATH_HH
#define XXG_MATH_HH

#include <cmath>

namespace xxg
{

struct Math
{
  static constexpr double pi = 3.14159265358979323846;
  static constexpr double pi_x_2 = pi * 2.0;
  static constexpr double half_pi = pi * 0.5;

  static double ceil(const double x)
  {
    return ::std::ceil(x);
  }

  static double floor(const double x)
  {
    return ::std::floor(x);
  }

  static double fabs(const double x)
  {
    return ::std::fabs(x);
  }

  static double cos(const double x)
  {
    return ::std::cos(x);
  }

  static double sin(const double x)
  {
    return ::std::sin(x);
  }

  static double acos(const double x)
  {
    return ::std::acos(x);
  }

  static double asin(const double x)
  {
    return ::std::asin(x);
  }
};

} // namespace xxg

#endif // XXG_MATH_HH
