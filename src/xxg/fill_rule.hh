/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef XXG_FILLRULE_HH
#define XXG_FILLRULE_HH

namespace xxg
{

enum class Fill_rule
{
  non_zero = 0,
  even_odd = 1
};

} // namespace xxg

#endif // XXG_FILLRULE_HH
