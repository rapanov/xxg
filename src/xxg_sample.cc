/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#include <cstddef>
#include <cstdint>
#include <vector>
#include <ppm_file.hh>
#include <xxg/bitmap_span_renderer.hh>
#include <xxg/ellipse.hh>
#include <xxg/math.hh>
#include <xxg/rasterizer.hh>
#include <xxg/rgb_888_blender.hh>
#include <xxg/transforming_outliner.hh>

struct ThickRectangle
{
  explicit ThickRectangle(
    const double width, const double height, const double pen_width)
    : width_(Math::fabs(width))
    , height_(Math::fabs(height))
    , pen_width_(Math::fabs(pen_width))
  {}

  template <class Outliner>
  void outline(Outliner&& outliner) const
  {
    const double half_width = width_ * 0.5;
    const double half_height = height_ * 0.5;
    const double half_pen_width = pen_width_ * 0.5;

    double x = half_width + half_pen_width;
    double y = half_height + half_pen_width;
    static_cast<Outliner&&>(outliner).move_to(x, y);
    x = - x;
    static_cast<Outliner&&>(outliner).line_to(x, y);
    y = - y;
    static_cast<Outliner&&>(outliner).line_to(x, y);
    x = - x;
    static_cast<Outliner&&>(outliner).line_to(x, y);

    x = half_width - half_pen_width;
    y = half_height - half_pen_width;
    static_cast<Outliner&&>(outliner).move_to(x, y);
    y = -y;
    static_cast<Outliner&&>(outliner).line_to(x, y);
    x = - x;
    static_cast<Outliner&&>(outliner).line_to(x, y);
    y = - y;
    static_cast<Outliner&&>(outliner).line_to(x, y);
  }

private:
  using Math = ::xxg::Math;

  double width_;
  double height_;
  double pen_width_;
};

struct ThickEllipse
{
  explicit ThickEllipse(
    const double radius_x, const double radius_y, const double pen_width)
    : small_ellipse_(
        0.0, 0.0, radius_x - pen_width * 0.5, radius_y - pen_width * 0.5, true)
    , large_ellipse_(
        0.0, 0.0, radius_x + pen_width * 0.5, radius_y + pen_width * 0.5, false)
  {}

  template <class Outliner>
  void outline(Outliner&& outliner) const
  {
    using ::xxg::Math;
    using ::xxg::Transformer;
    using Transforming_outliner = ::xxg::Transforming_outliner<Outliner>;

    Transforming_outliner t_o(static_cast<Outliner&&>(outliner));
    Transformer& transformer = t_o.transformer();

    small_ellipse_.outline(t_o);
    large_ellipse_.outline(t_o);

    transformer.rotate(Math::half_pi);

    small_ellipse_.outline(t_o);
    large_ellipse_.outline(t_o);
  }

private:
  using Ellipse = ::xxg::Ellipse;

  Ellipse small_ellipse_;
  Ellipse large_ellipse_;
};

struct Shape
{
  Shape()
    : rectangle_(170.0, 170.0, 8.5)
    , ellipse_(105.2, 33.0, 8.5)
  {}

  template <class Outliner>
  void outline(Outliner&& outliner) const
  {
    rectangle_.outline(static_cast<Outliner&&>(outliner));
    ellipse_.outline(static_cast<Outliner&&>(outliner));
  }

private:
  ThickRectangle rectangle_;
  ThickEllipse   ellipse_;
};

template <class Outliner, class Renderer>
static void draw_shapes(Outliner&& outliner, Renderer& renderer)
{
  using ::xxg::Math;
  using Fill_rule = ::xxg::Fill_rule;
  using ::xxg::Transformer;
  using Transforming_outliner = ::xxg::Transforming_outliner<Outliner>;

  Transforming_outliner t_o(static_cast<Outliner&&>(outliner));
  Transformer& transformer = t_o.transformer();
  const Shape shape;

  transformer.translate(120.0, 120.0);
  shape.outline(t_o);
  outliner.render(renderer, Fill_rule::non_zero);

  static_cast<Outliner&&>(outliner).reset();

  transformer.translate(230.0, 0.0);
  shape.outline(t_o);
  outliner.render(renderer, Fill_rule::even_odd);

  static_cast<Outliner&&>(outliner).reset();

  transformer.translate(-230.0, 230.0);
  transformer.rotate(Math::pi / 8.0, 62.0, 291.0);
  shape.outline(t_o);
  outliner.render(renderer, Fill_rule::even_odd);
}

int main()
{
  using Pixel = uint32_t;
  using Rasterizer = ::xxg::Rasterizer;
  using Blender = ::xxg::Rgb_888_blender;
  using Renderer = ::xxg::Bitmap_span_renderer<Pixel, Blender&>;

  static constexpr uint32_t width  = 500u;
  static constexpr uint32_t height = 500u;
  static constexpr uint32_t background_color = 0xffffffu; // White.
  static const size_t area =
    static_cast<size_t>(width) * static_cast<size_t>(height);

  std::vector<uint32_t> pixels(area);
  std::fill(pixels.begin(), pixels.end(), background_color);

  Rasterizer rasterizer;
  Blender blender;
  Renderer renderer(
    pixels.data(), blender, width, height,
    static_cast<size_t>(width) * sizeof(Pixel));
  renderer.set_color(0x0u); // Black.
  draw_shapes(rasterizer, renderer);

  std::vector<uint8_t> ppm(pixels.size() * 3u);
  uint8_t* ppm_p = ppm.data();

  for (const uint32_t pixel : pixels)
  {
    *ppm_p++ = static_cast<uint8_t>((pixel >> 16u) & 0xffu);
    *ppm_p++ = static_cast<uint8_t>((pixel >> 8u) & 0xffu);
    *ppm_p++ = static_cast<uint8_t>(pixel & 0xffu);
  }

  Ppm_file ppm_file("sample.ppm");
  ppm_file.write(width, height, ppm.data());

  return 0;
}