# Copyright 2020, Roman Panov (roman.a.panov@gmail.com).

add_library(xxg INTERFACE)

target_include_directories(
  xxg
  INTERFACE
  "${CMAKE_CURRENT_SOURCE_DIR}")

add_executable(
  xxg_sample
  xxg_sample.cc)

target_include_directories(
  xxg_sample
  INTERFACE
  "${CMAKE_CURRENT_SOURCE_DIR}")

target_link_libraries(
  xxg_sample
  xxg)
