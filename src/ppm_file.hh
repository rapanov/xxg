/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#ifndef PPMFILE_HH
#define PPMFILE_HH

#include <cstddef>
#include <cstdint>
#include <cstdio>

struct Ppm_file
{
  Ppm_file(const char* const file_path)
    : stream_(nullptr)
  {
#ifdef _MSC_VER

  const errno_t err = ::fopen_s(&stream_, file_path, "wb");

  if (err)
  {
    stream_ = nullptr;
  }

#else

  stream_ = std::fopen(file_path, "wb");

#endif
  }

  Ppm_file(const Ppm_file&) = delete;

  Ppm_file(Ppm_file&& x) noexcept
    : stream_(x.stream_)
  {
    x.stream_ = nullptr;
  }

  ~Ppm_file() noexcept(false)
  {
    close();
  }

  Ppm_file& operator = (const Ppm_file&) = delete;

  Ppm_file& operator = (Ppm_file&& x)
  {
    close();
    stream_ = x.stream_;
    x.stream_ = nullptr;
  }

  explicit operator bool () const noexcept
  {
    return stream_ != nullptr;
  }

  void write(
    const uint32_t width, const uint32_t height, const void* const data)
  {
    const size_t size =
      static_cast<size_t>(width) * static_cast<size_t>(height) * 3u;

    std::fprintf(stream_, "P6 %u %u 255 ", width, height);
    std::fwrite(data, 1u, size, stream_);
  }

private:
  void close()
  {
    if (stream_)
    {
      std::fflush(stream_);
      std::fclose(stream_);
    }
  }

  FILE* stream_;
};

#endif // PPMFILE_HH
